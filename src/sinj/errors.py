class CircularDependencyError(Exception):
    pass


class DependencyNotMappedError(Exception):
    pass


class DependencyNotFoundError(Exception):
    pass


class DependencyConflictError(Exception):
    pass
