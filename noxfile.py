import nox


@nox.session(python=["3.8", "3.9", "3.10", "3.11", "3.12", "3.13"])
def tests(session):
    session.install("-e", ".")
    session.run("python", "-m", "unittest", "discover", "./tests", "-v")
