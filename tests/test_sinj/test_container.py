from sinj import (
    Container,
    DependencyNotFoundError,
    CircularDependencyError,
    DependencyConflictError,
    DependencyNotMappedError,
)
import unittest

from . import init_debug


class TestContainer(unittest.TestCase):
    def test_container(self):
        class A:
            inject = "a"
            inject_dict = ["a_dict"]

            def __init__(self, b):
                self._b = b

            def a(self):
                return "a " + self._b.b()

        class B:
            inject = "b"

            def __init__(self, b_items):
                self._b_items = list(b_items.values())

            def b(self):
                return "-".join(sorted(i.b() for i in self._b_items))

        class B1:
            inject = "b1"
            inject_dict = "b_items"

            def b(self):
                return "b1"

        class B2:
            inject = "b2"
            inject_dict = ["b_items", "b_smthrandom"]

            def b(self):
                return "b2"

        ioc_container = Container()
        ioc_container.register(A)
        ioc_container.register(B)
        ioc_container.register(B1)
        ioc_container.register(B2)

        a = ioc_container.resolve("a")

        self.assertEqual(a.a(), "a b1-b2")

        self.assertRaises(DependencyNotFoundError, lambda: ioc_container.resolve("c"))
        self.assertIsNone(ioc_container.resolve("c", False))

        class C:
            def c(self):
                return "c"

        ioc_container.register(C, "c", "c_items")
        c = ioc_container.resolve("c", False)
        self.assertEqual(c.c(), "c")

        class D:
            def __init__(self, e):
                self._e = e

            def d(self):
                return "d"

        class E:
            def __init__(self, d):
                self._d = d

            def e(self):
                return "e"

        ioc_container.register(D, "d")
        ioc_container.register(E, "e")

        self.assertRaises(CircularDependencyError, lambda: ioc_container.resolve("d"))
        self.assertRaises(CircularDependencyError, lambda: ioc_container.resolve("e"))

        class F:
            def __init__(self, a, b=None, b_items=None):
                self._a = a
                self._b = b
                self._b_items = b_items

            def a(self):
                return self._a.a()

            def b(self):
                return self._b.b()

            def b_items_len(self):
                return len(self._b_items)

        ioc_container.register(F, "f")
        f = ioc_container.resolve("f")
        self.assertEqual(f.a(), "a b1-b2")
        self.assertEqual(f.b(), "b1-b2")
        self.assertEqual(f.b_items_len(), 2)

        class G:
            def __init__(self, a, bb=None):
                self._a = a
                self.bb = bb

            def g(self):
                return self._a.a()

        ioc_container.register(G, "g")
        g = ioc_container.resolve("g")
        self.assertEqual(g.g(), "a b1-b2")
        self.assertIsNone(g.bb)

        class H:
            def __init__(self, a, bb, c):
                pass

        ioc_container.register(H, "h")
        self.assertRaises(DependencyNotFoundError, lambda: ioc_container.resolve("h"))

        class I:
            wat = "wat"

        i = I()
        self.assertRaises(
            DependencyConflictError, lambda: ioc_container.register(I, "a")
        )
        self.assertRaises(DependencyConflictError, lambda: ioc_container.inject(i, "a"))
        self.assertRaises(
            DependencyConflictError, lambda: ioc_container.register(I, "b_items")
        )
        self.assertRaises(
            DependencyConflictError, lambda: ioc_container.inject(i, "b_items")
        )

        self.assertRaises(DependencyNotMappedError, lambda: ioc_container.register(I))
        self.assertRaises(DependencyNotMappedError, lambda: ioc_container.inject(I))

        class J:
            inject = "i"

        j = J()
        ioc_container.inject(i, "i")
        self.assertRaises(DependencyConflictError, lambda: ioc_container.inject(j))
        i = ioc_container.resolve("i")
        self.assertEqual(i.wat, "wat")

        # FYI this is invalid syntax in 3.7. mv to separate file maybe?
        class K:
            inject = "k"

            def __init__(self, a, b, /, c, bb=None):
                self.a = a
                self.b = b
                self.c = c
                self.bb = bb

        ioc_container.register(K)
        k = ioc_container.resolve("k")
        self.assertEqual(k.a.a(), "a b1-b2")
        self.assertEqual(k.b.b(), "b1-b2")
        self.assertEqual(k.c.c(), "c")
        self.assertIsNone(k.bb)

    def test_resolve_all(self):
        class A:
            inject = "a"

            def a(self):
                return "a"

        class B:
            inject = "b"

            def b(self):
                return "b"

        class AB:
            inject = "ab"

            def __init__(self, a, b):
                self.a = a
                self.b = b

            def ab(self):
                return self.a.a() + self.b.b()

        ioc_container = Container()
        ioc_container.register(A)
        ioc_container.register(B)
        ioc_container.register(AB)

        resolved = ioc_container.resolve_all()

        self.assertEqual(resolved["a"].a(), "a")
        self.assertEqual(resolved["b"].b(), "b")
        self.assertEqual(resolved["ab"].ab(), "ab")
