import os


if os.environ.get("DEBUG", "0") == "1":
    import debugpy

    debugpy.listen(5678)
    print("Waiting for debugger attach")
    debugpy.wait_for_client()
