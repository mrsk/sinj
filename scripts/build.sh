set -e

SCRIPT=$(realpath "$0")
SCRIPT_DIR=$(dirname "$SCRIPT")

cd -- "${SCRIPT_DIR}"
cd ../


VENV="venv-build"
if [ ! -d "${VENV}" ]; then
    echo "${VENV} does not exist."
    ./scripts/create-venv.sh ${VENV} -e . build twine
fi

source ${VENV}/bin/activate

rm -rf ./build
rm -rf ./dist

python -m build
twine check dist/*
