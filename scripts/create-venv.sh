set -e

SCRIPT=$(realpath "$0")
SCRIPT_DIR=$(dirname "$SCRIPT")

cd -- "${SCRIPT_DIR}"
cd ../


python3.12 -m venv $1
source $1/bin/activate
pip install pip --upgrade
pip install setuptools --upgrade
pip install wheel --upgrade

pip install ${@:2}
