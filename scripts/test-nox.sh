set -e

SCRIPT=$(realpath "$0")
SCRIPT_DIR=$(dirname "$SCRIPT")

cd -- "${SCRIPT_DIR}"
cd ../


VENV="venv-nox"
if [ ! -d "${VENV}" ]; then
  echo "${VENV} does not exist."
  ./scripts/create-venv.sh ${VENV} nox
fi

source ${VENV}/bin/activate

nox
