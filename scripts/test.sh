set -e

SCRIPT=$(realpath "$0")
SCRIPT_DIR=$(dirname "$SCRIPT")

cd -- "${SCRIPT_DIR}"
cd ../


VENV="venv-test"
if [ ! -d "${VENV}" ]; then
  echo "${VENV} does not exist."
  ./scripts/create-venv.sh ${VENV} -e . debugpy
fi

source ${VENV}/bin/activate

python -m unittest discover ./tests -v
